import random as rnd


def bubblesort(array):

    l = len(array)
    for i in range(len(array)):
        for j in range(l):
            if array[i] < array[j]:
                array[i], array[j] = array[j], array[i]
        l -= 1


#Test
if __name__=="__main__":

    s = []
    for i in range(10001):
        s.append(rnd.randint(1, 100))

    s = bubblesort(s)
