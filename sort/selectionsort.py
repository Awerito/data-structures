import random as rnd


def selectionsort(array):

    l = len(array)
    for i in range(len(array)):
        biggest = 0
        index = 0
        for j in range(l):
            if array[j] > biggest:
                biggest = array[j]
                index = j
        l -= 1
        temp = array[index]
        array[index] = array[l]
        array[l] = temp
    return array


#Test
if __name__=="__main__":

    s = []
    for i in range(10001):
        s.append(rnd.randint(1, 100))

    s = selectionsort(s)
