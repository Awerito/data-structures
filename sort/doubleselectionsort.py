import random as rnd


def selectionsort(array):

    l, b = len(array), 0
    for i in range(l):
        biggest = 0
        bindex = i
        smallest = 101
        sindex = i
        for j in range(b, l):
            if array[j] > biggest:
                biggest = array[j]
                bindex = j
            if array[j] < smallest:
                smallest = array[j]
                sindex = j
        l -= 1
        array[bindex], array[l] = array[l], array[bindex]

        array[sindex], array[b] = array[b], array[sindex]
        b += 1
    return array


#Test
if __name__=="__main__":

    s = []
    for i in range(10001):
            s.append(rnd.randint(1, 100))

    s = selectionsort(s)
