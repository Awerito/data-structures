from numba import jit


@jit
def last2bits(x): return x & 3


@jit
def hindex(index, order):

    positions = [
            [0, 0],
            [0, 1],
            [1, 1],
            [1, 0]
    ]

    tmp = positions[last2bits(index)]
    index = (index >> 2)

    x, y = tmp[0], tmp[1]

    n = 4
    while(n<=order):
        aux = n / 2

        if(last2bits(index) == 0):
            temp = x
            x = y
            y = temp
        elif(last2bits(index) == 1):
            x = x
            y = y + aux
        elif(last2bits(index) == 2):
            x = x + aux
            y = y + aux
        else:
            temp = y
            y = (aux - 1) - x
            x = (aux - 1) - temp
            x = x + aux

        index = index >> 2
        n *= 2

    return [int(x), int(y)]


if __name__=="__main__":

    import matplotlib.pyplot as plt

    horder = 6
    power = 2**horder

    x, y = [], []

    for i in range(power**2):
        aux = hindex(i, power)
        x.append(aux[0])
        y.append(aux[1])

    plt.plot(x, y)
    plt.show()
