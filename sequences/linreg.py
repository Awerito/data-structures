def mean(array): sum(array) / len(array)


def linreg(x, y):

    xmean, ymean = mean(x), mean(y)
    num, den = 0, 0
    for i in range(len(x)):
        aux = (x[i] - xmean) 
        num += aux * (y[i] - ymean)
        den += aux**2
    b = num / den
    a = ymean - b * xmean
    return [b, a]
