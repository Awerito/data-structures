from random import random as rnd
import matplotlib.pyplot as plt
from numba import jit
import numpy as np


@jit
def logistic(x, r): return r*x*(1 - x)


@jit
def conv(x0, r, niter):

    x = x0
    for i in range(niter):
        x = logistic(x, r)
    return x


if __name__=="__main__":

    iterations = 100
    a, b = 0, 4

    r_range = np.linspace(a, b, 10**6)
    x = []

    for r in r_range:
        x.append(conv(rnd(), r, iterations))

    plt.plot(r_range, x, ls='', marker=',')
    plt.show()
