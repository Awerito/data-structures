import matplotlib.pyplot as plt
from numba import jit
import numpy as np


@jit
def f(x, c): return x**2 + c


@jit
def mandelbrot(x, maxiter):

    """
        Return the left iteration before x become >2. Return 0 if maxiter is achieve.
    """

    c = x
    for i in range(maxiter):
        if abs(x) >= 2:
            return i
        x = f(x, c)
    return maxiter


def fractal_domain(dom, x_size, y_size, maxiter):

    """
        Return the fractal domain of the mandelbrot set
    """

    z       = np.zeros([x_size, y_size])
    x_range = np.linspace(dom[0], dom[1], x_size)
    y_range = np.linspace(dom[2], dom[3], y_size)

    for i, x in enumerate(x_range):
        for j, y in enumerate(y_range):
            z[i, j] = mandelbrot(complex(x, y), maxiter)

    fig = plt.figure(dpi=170, frameon=False)
    ax  = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    plt.imshow(z.T, cmap='hot', interpolation='bilinear', extent=[dom[0], dom[1], dom[2], dom[3]])
    plt.show()


if __name__=="__main__":

    maxiter        = 2048
    dom            = [-0.74877,-0.74872,0.0650609375,0.0650890625]
    x_size, y_size = 1922, 1080
    fractal_domain(dom, x_size, y_size, maxiter)
