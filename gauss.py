import numpy as np
from random import randint as rnd


def random_row(rows=4):
    """
        Return a List of random integers between 0 to 9 of length "rows".
        In  : random_row(4)
        Out : [4, 2, 5, 8] 
    """
    return [ rnd(0, 9) for _ in range(rows) ]


def random_square_matrix(cols=3, rows=4):
    """
        Return an array of "cols" columns with "rows" rows.
        In  : random_square_matrix(3)
        Out : np.array[[3, 7, 2],
                       [7, 0, 1],
                       [8, 5, 1]]
    """
    return np.array([ random_row(rows) for _ in range(cols) ])


if __name__=="__main__":
    matrix = random_square_matrix(cols=3, rows=3)
    for row in matrix:
        print(row)

    print()
    gauss = np.triu(matrix)
    for row in gauss:
        print(row)
