# :v
import pyglet as p5


window = p5.window.Window()
image = p5.resource.image('img/tux.png')

@window.event
def on_draw():
    window.clear()
    image.blit(0, 0)

p5.app.run()
