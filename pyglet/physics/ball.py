import pyglet
from random import randint as rnd


class Ball:

    def __init__(self, x, y, radius, x_speed=0, y_speed=0):

        self.x, self.y = x, y
        self.radius = radius
        self.x_speed, self.y_speed = x_speed, y_speed


    def draw(self):
    
        circle = pyglet.shapes.Circle(self.x, self.y, self.radius)
        circle.draw()


    def collition(self, width, height):
    
        if self.x + self.radius >= width or self.x - self.radius <= 0:
            self.x_speed *= -1

        if self.y + self.radius >= height or self.y - self.radius <= 0:
            self.y_speed *= -1


    def update(self, width, height):

        self.collition(width, height)
        self.x += self.x_speed
        self.y += self.y_speed
