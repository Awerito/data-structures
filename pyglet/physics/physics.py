from ball import Ball   
import pyglet


width, height = 680, 480
window        = pyglet.window.Window(width, height, 'Physics')
ball          = Ball(window.width//2, window.height//2, 10, x_speed=5, y_speed=3)


@window.event
def on_draw():

    window.clear()
    ball.draw()


def update(dt):

    ball.update(width, height)

pyglet.clock.schedule_interval(update, 1/60)
pyglet.app.run()
