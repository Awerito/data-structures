import pyglet
from random import choice, randint
from string import ascii_lowercase


class Character:

    def __init__(self, x, y, speed=0, first=False):
        
        self.x, self.y = x, y
        self.speed     = speed
        self.first     = first
        self.char      = choice(ascii_lowercase)


class Stream:

    def __init__(self, x=0, char_size=20):
    
        self.chars       = []
        self.total_chars = randint(1, 10)
        self.speed       = randint(5, 15) * (-1)

        for i in range(self.total_chars):
            char = Character(
                    x,
                    i*char_size,
                    speed=self.speed,
                    first=True if i==0 else False
            )
            self.chars.append(char)

    def draw(self, char_size):
    
        for char in self.chars:
            label = pyglet.text.Label(char.char,
                    font_name='Times New Roman', font_size=char_size//2,
                    color=(0,255,0,255) if char.first == False else (255,255,255,255),
                    x=char.x, y=char.y)
            label.draw()

    def update(self, limit):
    
        for char in self.chars:
            char.y = char.y + char.speed if char.y >= 0 else limit

            if 50 < randint(1, 100):
                char.char = choice(ascii_lowercase)
