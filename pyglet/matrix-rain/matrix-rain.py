from trail import Stream, Character   
import pyglet



width, height = 680, 480
window        = pyglet.window.Window(width, height, 'Matrix Rain')

char_size     = 36
matrices      = []
for x in range(0, window.width, char_size):
    matrices.append(Stream(x=x, char_size=char_size))


@window.event
def on_draw():

    window.clear()
    for matrix in matrices:
        matrix.draw(char_size)


def update(dt):

    for matrix in matrices:
        matrix.update(height)

pyglet.clock.schedule_interval(update, 1/60)
pyglet.app.run()
