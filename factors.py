def factors(n): return [ x from x in range(n) if n % x == 0 ]
